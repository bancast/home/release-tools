FROM golang:1.23.3 AS go
RUN go install gitlab.com/gitlab-org/release-cli/cmd/release-cli@latest

FROM rust:1.82.0 as rust
COPY --from=go /go/bin/release-cli /usr/local/bin/release-cli

USER root
RUN apt-get update && apt-get install -y jq
USER ubuntu
